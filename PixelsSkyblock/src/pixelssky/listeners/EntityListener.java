package pixelssky.listeners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import pixelssky.managers.PlayersManager;
import pixelssky.merchants.MerchantCategory;
import pixelssky.objects.Data;
import pixelssky.objects.Island;
import pixelssky.objects.SPlayer;
import pixelssky.utils.DistributedRandomNumberGenerator;

public class EntityListener implements Listener {
    public static DistributedRandomNumberGenerator drng = new DistributedRandomNumberGenerator();

    @EventHandler
    public void playerInteractEntityEvent(PlayerInteractEntityEvent event) {
        Player pl = event.getPlayer();
        SPlayer p = PlayersManager.getSPlayer(pl);
        try {
            if (p.getIsland().isAllowed(p, Island.PERMISSION_SHOP)) {
                String pnjName = event.getRightClicked().getName().substring(2);
                pl.openInventory(MerchantCategory.get(pnjName).getMainMenu(p.getIsland()));
            } else {
                pl.sendMessage("§cLe propriétaire de votre île a bloqué les shops :'(");
                pl.sendMessage("§6Peut-être que vous n'auriez pas dû acheter cette peluche licorne ...");
            }
        } catch (Exception ex) {
        }
    }

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        Location loc = event.getEntity().getLocation();
        if(event.getEntity() instanceof Player){
            return;
        }
        ItemStack reward = null;

        switch (drng.getDistributedRandomNumber()) {
            case 1:
                ;
            case 2:
                ;
            case 3:
                reward = new ItemStack(Material.EMERALD, 1);
                break;
            case 4:
                reward = new ItemStack(Material.EMERALD, 2);
                break;
            case 5:
                reward = new ItemStack(Material.EMERALD, 2);
                break;
            case 6:
                reward = new ItemStack(Material.EMERALD, 3);
                break;
            case 7:
                reward = new ItemStack(Material.DIRT, 1);
                break;
            case 8:
                reward = new ItemStack(Material.COBBLESTONE, 5);
                break;
            case 9:
                reward = new ItemStack(Material.EMERALD_BLOCK, 1);
                break;
            case 10:
                reward = new ItemStack(Material.DIAMOND, 1);
                break;
            default:
                break;
        }

        loc.getWorld().dropItemNaturally(loc, reward);
        if (event.getEntity().getKiller() instanceof Player) {
            SPlayer sp = PlayersManager.getSPlayer(event.getEntity().getKiller());
            Island i = sp.getIsland();
            if (i != null) {
                Data d = i.getData(Data.KILLED_MOBS + ":" + event.getEntity().getName());
                if (d != null) {
                    d.add(1);
                } else {
                    i.addOrSetData(Data.KILLED_MOBS + ":" + event.getEntity().getName(), 1);
                }
            }
        }

        loc.getWorld().dropItemNaturally(loc, reward);

    }


}
