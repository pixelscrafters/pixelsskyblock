package pixelssky.rewards;

import com.google.gson.*;

import java.lang.reflect.Type;

public class RewardCreator implements JsonDeserializer<Reward> {

    @Override
    public Reward deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String jsonString = json.toString();
        Gson gson = new GsonBuilder().create();
        if (json.getAsJsonObject().has("command")) {
            return gson.fromJson(jsonString, CommandReward.class);
        } else if (json.getAsJsonObject().has("dName")) {
            return gson.fromJson(jsonString, StatsReward.class);
        } else if (json.getAsJsonObject().has("material")) {
            return gson.fromJson(jsonString, GiveReward.class);
        } else {
            return null;
        }
    }
}