package pixelssky.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pixelssky.managers.ChallengesManager;

public class LoaderCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            p.sendMessage("Skyblock : reloading challenges");
            ChallengesManager.challenges.clear();
            ChallengesManager.initJsonChallenges(ChallengesManager.CHALLENGES_PATH, null);
            p.sendMessage(ChallengesManager.challenges.size() + " challenges loaded.");
        }
        return true;
    }
}
