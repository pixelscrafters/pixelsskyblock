package pixelssky.commands;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pixelssky.managers.PlayersManager;
import pixelssky.objects.Island;
import pixelssky.objects.SPlayer;
import pixelssky.utils.Inventories;

public class ChallengeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
        if (arg0 instanceof Player) {
            Player pl = (Player) arg0;
            SPlayer p = PlayersManager.getSPlayer(pl);
            if (p.getIsland() != null) {
                if (p.getIsland().isAllowed(p, Island.PERMISSION_CHALLENGE)) {
                    pl.openInventory(Inventories.getChallengesMainInventory(p.getIsland()));
                    pl.playSound(pl.getLocation(), Sound.BLOCK_NOTE_BLOCK_XYLOPHONE, 100, 1000);
                } else {
                    pl.sendTitle("§cSCROGNEUGNEU !", "§cVous n'avez pas la permission :(", 10, 20, 10);
                    pl.playSound(pl.getLocation(), Sound.ENTITY_ENDER_DRAGON_GROWL, 100, 1000);
                }
            }
        } else {
            arg0.sendMessage("La console ne peut pas effectuer cette commande.");
        }
        return true;
    }

}
