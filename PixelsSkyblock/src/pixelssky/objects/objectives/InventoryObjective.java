package pixelssky.objects.objectives;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import pixelssky.objects.Island;

public class InventoryObjective extends Objective {
    Material objMaterial;
    int objQuantity;
    boolean take;
    int current_qte = 0;


    public InventoryObjective(Material m, int objQuantity, boolean take) {
        super(Objective.ONISLAND);
        this.objMaterial = m;
        this.objQuantity = objQuantity;
        this.take = take;
    }

    @Override
    public boolean check(Player p, Island i) {
        return (p.getInventory().containsAtLeast(new ItemStack(objMaterial, objQuantity), objQuantity));
    }

    @Override
    public void run(Player p, Island i) {
        if (take) {
            p.getInventory().removeItem(new ItemStack(objMaterial, objQuantity));
        }
    }

    @Override
    public String getFailMessage(Player p) {
        return "§e-▶§4" + objQuantity + " §citems de §4" + objMaterial.toString() + " §cest/sont requis";

    }

    @Override
    public String getDescription() {
        return "§e-▶§4" + objQuantity + " §citems de §4" + objMaterial.toString();
    }

}
