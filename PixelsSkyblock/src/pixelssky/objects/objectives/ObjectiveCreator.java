package pixelssky.objects.objectives;

import com.google.gson.*;

import java.lang.reflect.Type;

public class ObjectiveCreator implements JsonDeserializer<Objective> {

    @Override
    public Objective deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String jsonString = json.toString();
        Gson gson = new GsonBuilder().create();
        try {
            if (json.getAsJsonObject().has("objOnIslandMaterial")) {
                return gson.fromJson(jsonString, OnislandObjective.class);
            } else if (json.getAsJsonObject().has("objMaterial")) {
                return gson.fromJson(jsonString, InventoryObjective.class);
            } else if (json.getAsJsonObject().has("stat_ID")) {
                return gson.fromJson(jsonString, StatsObjective.class);
            } else {
                return null;
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }
}
