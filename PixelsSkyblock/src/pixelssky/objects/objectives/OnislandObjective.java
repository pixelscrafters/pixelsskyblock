package pixelssky.objects.objectives;

import com.sk89q.worldedit.entity.Entity;
import com.sk89q.worldedit.util.Countable;
import com.sk89q.worldedit.world.block.BlockType;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import pixelssky.main.Main;
import pixelssky.objects.Island;
import pixelssky.utils.WEManager;

import java.util.List;

public class OnislandObjective extends Objective {
    boolean blockTypeOrEntity = false; //0: Blocks 1: entités
    Material objOnIslandMaterial;
    int objOnIslandQuantity;
    String it_name;
    int calculated_nb;

    public OnislandObjective(boolean type, String mat_ID, int quantity) {
        super(Objective.ONISLAND);
        this.blockTypeOrEntity = type;
        if (!type)
            this.objOnIslandMaterial = Material.getMaterial(mat_ID);
        this.it_name = mat_ID;
        this.objOnIslandQuantity = quantity;

    }

    @Override
    public boolean check(Player p, Island i) {
        if (!blockTypeOrEntity) {
            List<Countable<BlockType>> blocks = WEManager.count(Bukkit.getWorld(Main.PLAY_WORLD), i.getEdges().get(0), i.getEdges().get(1));
            for (Countable<BlockType> b : blocks) {
                try {
                    System.out.println("USED : " + b.getID().getId() + " NAME : " + b.getID().getName());
                    String matName = b.getID().getId().toUpperCase().replaceAll(" ", "_").replaceAll("MINECRAFT:", "");
                    System.out.println("CALCULATED : " + matName);
                    if (Material.getMaterial(matName).equals(objOnIslandMaterial) && b.getAmount() >= objOnIslandQuantity) {
                        return true;
                    } else if (Material.getMaterial(matName).equals(objOnIslandMaterial)) {
                        calculated_nb = b.getAmount();
                    }
                } catch (Exception ex) {

                }
            }
            return false;
        } else {
            try {

                List<Entity> es = (List<Entity>) WEManager.count_entities(Bukkit.getWorld(Main.PLAY_WORLD), i.getEdges().get(0), i.getEdges().get(1));
                int qte = 0;
                for (Entity e : es) {
                    try {
                        if (e.getState().getType().getName().toUpperCase().replaceAll(" ", "_").replaceAll("MINECRAFT:", "").equalsIgnoreCase(it_name)) {
                            qte += 1;
                        }
                    } catch (Exception ex) {

                    }
                }
                calculated_nb = qte;
                if (qte >= objOnIslandQuantity) {
                    return true;
                }
            } catch (Exception ex) {
            }

        }
        return false;
    }

    @Override
    public void run(Player p, Island i) {
        // Rien à faire : du moment que le check est validé

    }

    @Override
    public String getFailMessage(Player p) {
        if (blockTypeOrEntity) {
            return "§e-▶§6" + calculated_nb + "/§l" + objOnIslandQuantity + " §eentité(s) de §6" + it_name + " §cest/sont posée(s) sur l'île";

        } else {
            if (!(new ItemStack(objOnIslandMaterial, objOnIslandQuantity).toString()).equals("Air"))
                return "§e-▶§6" + calculated_nb + "/§l" + objOnIslandQuantity + " §eitems de §6" + objOnIslandMaterial.toString() + " §cest/sont posé(s) sur l'île";
            else
                return "§e-▶§6" + calculated_nb + "/§l" + objOnIslandQuantity + " §eitems de §6" + objOnIslandMaterial.name() + " §cest/sont posé(s) sur l'île";

        }
    }

    @Override
    public String getDescription() {
        if (blockTypeOrEntity) return "§e-▶§6" + objOnIslandQuantity + " §eentités de §6" + it_name + " §eposée(s) sur l'île";
        if (new ItemStack(objOnIslandMaterial, objOnIslandQuantity).toString().equals("Air"))
            return "§e-▶§6" + objOnIslandQuantity + " §eitems de §6" + objOnIslandMaterial.toString() + " §eposé(s) sur l'île";
        return "§e-▶§6" + objOnIslandQuantity + " §eitems de §6" + objOnIslandMaterial.toString() + " §eposé(s) sur l'île";

    }


}
