package pixelssky.managers;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class FileManager {
    public static void saveFile(String path, ArrayList<String> text) {
        PrintWriter ecri;
        try {
            ecri = new PrintWriter(new FileWriter(path));
            for (String str : text) {
                ecri.println(str);
            }
            ecri.flush();
            ecri.close();
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    public static ArrayList<String> readAllText(String fichier) {
        ArrayList<String> txt = new ArrayList<String>();

        try {
            InputStream ips = new FileInputStream(fichier);
            InputStreamReader ipsr = new InputStreamReader(ips, StandardCharsets.UTF_8);
            BufferedReader br = new BufferedReader(ipsr);
            String ligne;
            while ((ligne = br.readLine()) != null) {
                txt.add(ligne);
            }
            br.close();
        } catch (Exception e) {
            //System.out.println(e.toString());
        }
        return txt;
    }

    public static String readAllTextToString(String fichier){
        return String.join("\n", readAllText(fichier));
    }
}
