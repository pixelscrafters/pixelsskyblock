package pixelssky.managers;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import pixelssky.objects.Challenge;
import pixelssky.objects.objectives.*;
import pixelssky.rewards.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;

public class ChallengesManager {
    public static final String CHALLENGES_PATH = "plugins/PixelsSky/Challenges";

    public static ArrayList<Challenge> challenges = new ArrayList<Challenge>();
    public static int number_of_challenges = 0;

    /**
     * Method used to load all the challenges files from a folder.
     * @param path The location of the challenges files. Can be set on NULL to load from default path.
     */
    public static void initJsonChallenges(String path, Challenge parent){
        if(parent == null){
            number_of_challenges = 0;
        }
        if(Strings.isNullOrEmpty(path)) {
            path = CHALLENGES_PATH;
        }
        System.out.println("[PIXELS SKY] Loading challenges from : " + path);
        File folder = new File(path);
        for (File f : folder.listFiles()){
            try {
                String json = FileManager.readAllTextToString(f.getAbsolutePath());
                GsonBuilder gsonbuilder = new GsonBuilder();
                gsonbuilder.registerTypeAdapter(Objective.class, new ObjectiveCreator());
                gsonbuilder.registerTypeAdapter(Reward.class, new RewardCreator());
                Gson gson = gsonbuilder.create();
                Challenge challenge = gson.fromJson(json, Challenge.class);
                if(challenge != null) {
                    if(parent == null) {
                        challenges.add(challenge);
                    }else {
                        parent.getSubChallenges().add(challenge);
                    }
                    //Loads the sub challenges
                    if(challenge.isCategory()){
                        initJsonChallenges(path + "/" + challenge.getName(), challenge);
                    } else {
                        number_of_challenges++;
                    }
                }
            } catch(Exception ex) {
                ex.printStackTrace();
                System.out.println("Could not load " + f.getPath() + " : bad file format.\n" + ex.toString());
            }
        }
        //Order
        if(parent == null) {
            order(challenges);
        } else {
            order(parent.getSubChallenges());
        }
    }

    public static Challenge getChallenge(String name) {
        for (Challenge c : challenges) {
            if (c.getName().equals(name)) {
                return c;
            }
        }
        return null;
    }

    public static void order(ArrayList<Challenge> challengesList){
        Collections.sort(challengesList, Challenge.COMPARE_BY_NAME);
    }
}
