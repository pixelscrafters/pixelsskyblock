package pixelssky.tabcompleter;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//https://bukkit.org/threads/easy-no-api-setting-up-custom-tab-completion.299956/

public class IsTabCompleter implements TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        if (cmd.getName().equalsIgnoreCase("is")) {
            if (args.length <= 1) {
                return Arrays.asList("help", "create", "c", "h", "sethome", "top", "leave", "stats", "name", "level",
                        "invite", "accept", "s", "tuto");
            } else if (args[0].toLowerCase().equals("invite") && args.length <= 2) {
                List<String> list = new ArrayList<>();
                for (Player p : Bukkit.getOnlinePlayers()) {
                    list.add(p.getDisplayName());
                }
                return list;
            }
        }
        return null;
    }
}
