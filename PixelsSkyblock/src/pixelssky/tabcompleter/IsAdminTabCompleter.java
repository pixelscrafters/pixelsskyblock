package pixelssky.tabcompleter;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.EntityType;
import pixelssky.managers.IslandsManager;
import pixelssky.merchants.MerchantCategory;
import pixelssky.merchants.MerchantInventory;
import pixelssky.objects.Island;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IsAdminTabCompleter implements TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        if (cmd.getName().equalsIgnoreCase("pxs")) {
            if (args.length <= 1) {
                return Arrays.asList("list", "goto", "protection", "clean", "ram", "save", "shop");
            } else if (args[0].equals("goto") && args.length <= 2) {
                List<String> list = new ArrayList<>();
                for (Island i : IslandsManager.islands) {
                    try {
                        if (i.getName().startsWith(args[1])) {
                            list.add(i.getName());
                        }
                    } catch (Exception ex) {

                    }
                }
                return list;
            } else if (args[0].equals("shop")) {
                if (args.length <= 2) {
                    return Arrays.asList("add", "addlvl", "set", "save");
                } else if (args[1].equals("add")) {
                    if (args.length <= 3) {
                        return Arrays.asList("categorie");
                    } else if (args.length <= 4) {
                        List<String> list = new ArrayList<>(Arrays.asList(""));
                        for (EntityType t : EntityType.values()) {
                            if (t.name().startsWith(args[3])) {
                                list.add(t.name());
                            }
                        }
                        return list;
                    }
                } else if (args[1].equals("set")) {
                    if (args.length <= 3) {
                        List<String> list = new ArrayList<>();
                        for (MerchantCategory m : MerchantCategory.mCategories) {
                            if (m.categName.startsWith(args[2])) {
                                list.add(m.categName);
                            }
                        }
                        return list;
                    } else if (args.length <= 4) {
                        MerchantCategory m = MerchantCategory.get(args[2]);
                        ArrayList<String> list = new ArrayList<String>();
                        for (MerchantInventory mi : m.merchants) {
                            list.add(mi.getLevel() + "");
                        }
                        return list;
                    } else if (args.length <= 5) {
                        List<String> list = new ArrayList<>();
                        for (Material m : Material.values()) {
                            if (m.name().startsWith(args[4])) {
                                list.add(m.name());
                            }
                        }
                        return list;
                    } else if (args.length <= 6) {
                        return Arrays.asList("quantite");
                    } else if (args.length <= 7) {
                        return Arrays.asList("prix");
                    }
                } else if (args[1].equals("addlvl")) {
                    if (args.length <= 3) {
                        List<String> list = new ArrayList<>();
                        for (MerchantCategory m : MerchantCategory.mCategories) {
                            if (m.categName.startsWith(args[2])) {
                                list.add(m.categName);
                            }
                        }
                        return list;
                    } else if (args.length <= 4) {
                        return Arrays.asList("level");
                    }
                }
            } else if (args[0].equals("purge") && args.length <= 2) {
                return Arrays.asList("verif", "execute", "force");
            }
        }
        return null;
    }

}
